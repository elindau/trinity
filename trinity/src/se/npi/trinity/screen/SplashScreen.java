package se.npi.trinity.screen;

import se.npi.trinity.TrinityGame;
import se.npi.trinity.resources.Resources;
import se.npi.trinity.tween.AlphaAccessor;
import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenEquations;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SplashScreen implements Screen {

	private Texture mSplashTexture;
	private Sprite mSplashSprite;
	private SpriteBatch mSpriteBatch;
	private TrinityGame mGame;
	private TweenManager mTweenManager;
	private TweenCallback mTweenCallback;
	private boolean mFirst, mTweenDone;
	
	public SplashScreen(TrinityGame game) {
		mGame = game;
		
		// Start loading resources
		Resources.INSTANCE.loadGameResources();
		mFirst = true;
		mTweenDone = false;
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		if(mFirst) {
			mFirst = false;
			Tween.to(mSplashSprite, AlphaAccessor.ALPHA, 2f)
			.target(1)
			.ease(TweenEquations.easeInQuad)
			.repeatYoyo(1, .4f)
			.setCallback(mTweenCallback)
			.setCallbackTriggers(TweenCallback.COMPLETE)
			.start(mTweenManager);
			
		}
		
		mTweenManager.update(delta);
		if(Resources.INSTANCE.getAssetManager().update()) {
			if(mTweenDone) {
				if(Resources.INSTANCE.setResourceInstances()) {
					// Now it's safe to change screen
					mGame.setScreen(new GameScreen(mGame));
				}
			}
		}
		
		mSpriteBatch.begin();
		mSplashSprite.draw(mSpriteBatch);
		mSpriteBatch.end();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		mSplashTexture = new Texture("gfx/splash.png");
		mSplashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		mSplashSprite = new Sprite(mSplashTexture);
		mSplashSprite.setColor(1, 1, 1, 0); // Alpha zero
		mSplashSprite.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		
		mSpriteBatch = new SpriteBatch();
		
		Tween.registerAccessor(Sprite.class, new AlphaAccessor());
		mTweenManager = new TweenManager();
		
		mTweenCallback = new TweenCallback() {
			
			@Override
			public void onEvent(int type, BaseTween<?> source) {		
				tweenCompleted();
			}
		};
	}
	
	// Just for setting newScreen
	private void tweenCompleted() {
		mTweenDone = true;
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

}
