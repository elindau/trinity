package se.npi.trinity.screen;

import com.badlogic.gdx.math.MathUtils;
import se.npi.trinity.Camera;
import se.npi.trinity.controller.WorldController;
import se.npi.trinity.model.PhysicsWorld;
import se.npi.trinity.model.WorldModel;
import se.npi.trinity.resources.Resources;
import se.npi.trinity.view.WorldView;
import box2dLight.DirectionalLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;

public class GameScreen extends BaseScreen {
	
	private Camera mCamera;
	
	private WorldController mWorldController;
	private WorldModel mWorldModel;
	private WorldView mWorldView;
	
	public GameScreen(Game game) {
		super(game);

        float h = Gdx.graphics.getHeight();
        float w = Gdx.graphics.getWidth();
		mCamera = new Camera(Resources.INSTANCE.mapHelper, h, w);
		// Just set the camera to original pos
		mCamera.translate(Camera.CAMERA_WIDTH / 2f, Camera.CAMERA_HEIGHT / 2f);
		mCamera.update();
		
		mWorldModel = new WorldModel();
		mWorldController = new WorldController(mCamera, mWorldModel.getPlayer());
		mWorldView = new WorldView(mWorldModel, mCamera);
		
		// Adding controller
		Controllers.addListener(mWorldController);
		
		// Attach the player to the camera
		mCamera.attatchToEntity(mWorldModel.getPlayer());



        /*
		DirectionalLight dl = new DirectionalLight(mRayHandler, 1000, new Color(0f, 0f, 0f, 0.3f), -90);
		dl.setXray(false);

        DirectionalLight dl2 = new DirectionalLight(mRayHandler, 199, new Color(1f, 1f, 1f, 0.5f), -90);
        dl2.setXray(true);
        */
		
		int i = 0;
		for(Controller ctrl : Controllers.getControllers()) {
			System.out.println("#" + i++ + ": " + ctrl.getName());
		}
	}
	

	@Override
	public void update(float delta) {
		mWorldController.processCurrentInput();
		mWorldModel.update(delta);
		mCamera.update(delta);
	}

	@Override
	public void draw(float delta) {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		mWorldView.draw(delta);
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		// Make the controller the inputprocessor
		Gdx.input.setInputProcessor(mWorldController);
		Gdx.input.setCatchBackKey(true);
	}

	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
	}
}
