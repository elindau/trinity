package se.npi.trinity.screen;

import se.npi.trinity.TrinityGame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

public abstract class BaseScreen implements Screen {

	private TrinityGame mGame;
	
	public BaseScreen(Game game) {
		if(game instanceof TrinityGame) {
			mGame = (TrinityGame) game;
		} else {
			// TODO: Throw some kind of error
		}
	}
	
	public TrinityGame getGame() { return mGame; }
	
	@Override
	public void render(float delta) {
		update(delta);
		draw(delta);
	}
	
	public abstract void update(float delta);
	public abstract void draw(float delta);

}
