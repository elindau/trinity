package se.npi.trinity.controller;

import java.util.HashMap;
import java.util.Map;

import se.npi.trinity.Camera;
import se.npi.trinity.model.entity.Player;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;

public class WorldController implements InputProcessor, ControllerListener {

	public static boolean DEBUG = false;
	
	private Camera mCamera;
	private Player mPlayer;
	private boolean mNext = true;
	
	public enum MapKeys {
		LEFT, RIGHT, JUMP, SHAPECHANGE;
	}
	
	private static final Map<MapKeys, Boolean> mKeys = new HashMap<MapKeys, Boolean>();
	static {
		mKeys.put(MapKeys.LEFT, false);
		mKeys.put(MapKeys.RIGHT, false);
		mKeys.put(MapKeys.JUMP, false);
		mKeys.put(MapKeys.SHAPECHANGE, false);
	}
	
	public WorldController(Camera cam, Player player) {
		mCamera = cam;
		mPlayer = player;
	}
	
	/**
	 * Handles what's currently in the map
	 * and act thereafter
	 */
	public void processCurrentInput() {
		
		if(mKeys.get(MapKeys.RIGHT)) {
			// Check that we're not overriding left
			if(!(mKeys.get(MapKeys.LEFT)))
				mPlayer.moveRight();
		} else if(mKeys.get(MapKeys.LEFT)) {
			if(!(mKeys.get(MapKeys.RIGHT)))
				mPlayer.moveLeft();
		} else {
			// None of the above is true,
			// thus velocity 0
			mPlayer.standStill();
		}
		
		if(mKeys.get(MapKeys.JUMP)) {
			// Do jump
			mPlayer.jump();
		} else {
			mPlayer.reEnableJump();
		}
		
		if(mKeys.get(MapKeys.SHAPECHANGE)) {
			mPlayer.tryChangeShape(mNext);
		} else {
			mPlayer.reEnableShapeChange();
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		switch(keycode) {
		case Keys.LEFT:
		case Keys.A:
			mKeys.get(mKeys.put(MapKeys.LEFT, true));
			break;
		case Keys.RIGHT:
		case Keys.D:
			mKeys.get(mKeys.put(MapKeys.RIGHT, true));
			break;
		case Keys.SPACE:
			mKeys.get(mKeys.put(MapKeys.JUMP, true));
			break;
		case Keys.E:
			mKeys.get(mKeys.put(MapKeys.SHAPECHANGE, true));
			mNext = true;
			break;
		case Keys.Q:
			mKeys.get(mKeys.put(MapKeys.SHAPECHANGE, true));
			mNext = false;
			break;
		}
		return true;
	}

	@Override
	public boolean keyUp(int keycode) {
		switch(keycode) {
		case Keys.LEFT:
		case Keys.A:
			mKeys.get(mKeys.put(MapKeys.LEFT, false));
			break;
		case Keys.RIGHT:
		case Keys.D:
			mKeys.get(mKeys.put(MapKeys.RIGHT, false));
			break;
		case Keys.SPACE:
			mKeys.get(mKeys.put(MapKeys.JUMP, false));
			break;
		case Keys.E:
			mKeys.get(mKeys.put(MapKeys.SHAPECHANGE, false));
			break;
		case Keys.Q:
			mKeys.get(mKeys.put(MapKeys.SHAPECHANGE, false));
			break;
		case Keys.NUM_1:
			DEBUG = !DEBUG;
			break;
		}
		return true;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		mCamera.zoom += amount;
		return true;
	}
	
	/**
	 * CONTROLLER INTERFACE
	 */

	@Override
	public boolean accelerometerMoved(Controller arg0, int arg1, Vector3 arg2) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean axisMoved(Controller controller, int axisIndex, float value) {
		System.out.println("#" + indexOf(controller) + ", axis " + axisIndex + ": " + value);
		switch(axisIndex) {
		case 1: // X-axis?
			if(value > 0.9) {
				// Right
				mKeys.get(mKeys.put(MapKeys.LEFT, false));
				mKeys.get(mKeys.put(MapKeys.RIGHT, true));
			} else if (value < 0.9 && value > 0.01) {
				mKeys.get(mKeys.put(MapKeys.LEFT, false));
				mKeys.get(mKeys.put(MapKeys.RIGHT, false));
			} else if(value < -0.01 && value > -0.9) {
				mKeys.get(mKeys.put(MapKeys.LEFT, false));
				mKeys.get(mKeys.put(MapKeys.RIGHT, false));
			} else if(value < -0.9) {
				// Left
				mKeys.get(mKeys.put(MapKeys.RIGHT, false));
				mKeys.get(mKeys.put(MapKeys.LEFT, true));
			}
			break;
		}
		return false;
	}

	
    private int indexOf(Controller controller) {
        return Controllers.getControllers().indexOf(controller, true);
    }
    
	@Override
	public boolean buttonDown(Controller controller, int buttonIndex) {
		
		System.out.println("#" + indexOf(controller) + ", button " + buttonIndex + " down");
		
		switch(buttonIndex) {
		case 0: // A
			mKeys.get(mKeys.put(MapKeys.JUMP, true));
			break;
		case 1: // B
			DEBUG = !DEBUG;
			break;
		case 4: // LB
			mKeys.get(mKeys.put(MapKeys.SHAPECHANGE, true));
			mNext = false;
			break;
		case 5: // RB
			mKeys.get(mKeys.put(MapKeys.SHAPECHANGE, true));
			mNext = true;
			break;
		}
		return true;
	}

	@Override
	public boolean buttonUp(Controller arg0, int buttonIndex) {
		switch(buttonIndex) {
		case 0: // A
			mKeys.get(mKeys.put(MapKeys.JUMP, false));
			break;
		case 1: // B
			mKeys.get(mKeys.put(MapKeys.SHAPECHANGE, false));
			break;
		case 4: // LB
			mKeys.get(mKeys.put(MapKeys.SHAPECHANGE, false));
			break;
		case 5: // RB
			mKeys.get(mKeys.put(MapKeys.SHAPECHANGE, false));
			break;
		}
		return true;
	}

	@Override
	public void connected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disconnected(Controller arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean povMoved(Controller controller, int povIndex, PovDirection value) {
		System.out.println("#" + indexOf(controller) + ", pov " + povIndex + ": " + value);
		return false;
	}

	@Override
	public boolean xSliderMoved(Controller controller, int sliderIndex, boolean value) {
		System.out.println("#" + indexOf(controller) + ", x slider " + sliderIndex + ": " + value);
		return false;
	}

	@Override
	public boolean ySliderMoved(Controller controller, int sliderIndex, boolean value) {
		System.out.println("#" + indexOf(controller) + ", y slider " + sliderIndex + ": " + value);
		return false;
	}
	
}
