package se.npi.trinity;

import se.npi.trinity.screen.SplashScreen;

import com.badlogic.gdx.Game;

public class TrinityGame extends Game {

	
	@Override
	public void create() {		
		setScreen(new SplashScreen(this));
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void render() {
		super.render();
	}

	@Override
	public void resize(int width, int height) {

    }

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
