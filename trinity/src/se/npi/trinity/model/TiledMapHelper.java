package se.npi.trinity.model;

import java.util.*;

import com.badlogic.gdx.maps.MapLayer;
import se.npi.trinity.model.entity.PassiveEntity;
import se.npi.trinity.model.entity.Tile;
import se.npi.trinity.model.entity.Tile.TileType;

import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.EdgeShape;


public class TiledMapHelper {
	
	private static final String PROPERTIES_TYPE = "type";
	private static final String TYPE_GROUND = "ground";
    private static final String TYPE_WATER = "water";
	private static final String TYPE_FILL = "fill";
    private static final String TYPE_CHECKPOINT = "checkpoint";

    public static final int LAYER_TILES = 0;
    public static final int LAYER_WATER = 1;
    public static final int LAYER_FOREGROUND = 2;
    public static final int LAYER_BACKGROUND = 3;
    public static final int LAYER_OBJECTS = 4;

	private final TiledMap mMap;
	
	private TiledMapTileLayer mLayer;
    private TiledMapTileLayer mWaterLayer;
    private TiledMapTileLayer mObjectLayer;

	private List<Tile> mTiles;
    private List<Tile> mObjectTiles;
	private Map<Integer, List<Integer>> mMendXTiles;
    private LinkedList<Vector2> mMendYTiles;
    private ArrayList<Vector2> mWaterTiles;
	
	public TiledMapHelper(TiledMap map) {
		mMap = map;
		mLayer = (TiledMapTileLayer) map.getLayers().get(LAYER_TILES);
        mWaterLayer = (TiledMapTileLayer) map.getLayers().get(LAYER_WATER);
        mObjectLayer = (TiledMapTileLayer) map.getLayers().get(LAYER_OBJECTS);

		mTiles = new ArrayList<>();
        mObjectTiles = new ArrayList<>();
		mMendXTiles = new HashMap<>();
        mMendYTiles = new LinkedList<>();
        mWaterTiles = new ArrayList<>();

		loadCollisions();
	}
	
	public TiledMap getMap() {
		return mMap;
	}
	
	public int getHeight() {
		return (int) (mLayer.getHeight());// * mLayer.getTileHeight());
	}
	
	public int getWidth() {
		return (int) (mLayer.getWidth());// * mLayer.getTileWidth());
	}
	
	public boolean loadCollisions() {
		
		for(int x = 0; x < mLayer.getWidth(); x++) {
			for(int y = 0; y < mLayer.getHeight(); y++) {
				Cell c = mLayer.getCell(x, y);

				if(c != null) {
					TiledMapTile t = mLayer.getCell(x, y).getTile();
					
					String type = (String) t.getProperties().get(PROPERTIES_TYPE);

                    if(type != null) {

                        switch(type) {
                            case TYPE_GROUND:
                                // Adding all tiles to the a hashmap were the key is the y-pos
                                List<Integer> temp = mMendXTiles.get(y);
                                if(temp == null) {
                                    temp = new ArrayList<>();
                                }
                                temp.add(x);

                                mMendXTiles.put(y, temp);
                                break;
                            default:
                                break;
                        }
                    }
				}
			}
		}

        for(int x = 0; x < mWaterLayer.getWidth(); x++) {
            for(int y = 0; y < mWaterLayer.getHeight(); y++) {
                Cell c = mWaterLayer.getCell(x, y);

                if(c != null) {
                    TiledMapTile t = mWaterLayer.getCell(x, y).getTile();

                    String type = (String) t.getProperties().get(PROPERTIES_TYPE);

                    if(type != null) {

                        switch(type) {
                            case TYPE_WATER:
                                mWaterTiles.add(new Vector2(x, y));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        for(int x = 0; x < mObjectLayer.getWidth(); x++) {
            for(int y = 0; y < mObjectLayer.getHeight(); y++) {
                Cell c = mObjectLayer.getCell(x, y);

                if(c != null) {
                    TiledMapTile t = mObjectLayer.getCell(x, y).getTile();

                    String type = (String) t.getProperties().get(PROPERTIES_TYPE);

                    if(type != null) {

                        switch(type) {
                            case TYPE_CHECKPOINT:
                                mObjectTiles.add(Tile.valueOf(TileType.CHECKPOINT, new Vector2(x, y)));
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        }

		
		// Heh, time complexity anyone?
		// Alright, every added Y in the map
		for(int y : mMendXTiles.keySet()) {
			// Get the X's where we have tiles
			ArrayList<Integer> xs = (ArrayList<Integer>) mMendXTiles.get(y);
			// Is it more than one tile	
			ArrayList<Integer> chunk = new ArrayList<>();

			for(int i = 0; i < xs.size(); i++) {
				// First
				if(chunk.size() == 0) {
					// Add this tile
					chunk = new ArrayList<>();
					chunk.add(xs.get(i));
				} else if((chunk.get(chunk.size() - 1) - xs.get(i)) == -1) {
					// If the difference is just one, it means the tiles are next to each other
					// and can be added to the chunk
					chunk.add(xs.get(i));
				} else {
					// The difference is bigger, add current chunk, clear the chunk
					// and decrease i by one to get to this case again
					float startX = chunk.get(0);
					float endX = chunk.get(chunk.size() -1);					
					i--;
                    if(chunk.size() == 1) {
                        // Here's our singles with potential of mending in Y
                        mMendYTiles.addLast(new Vector2(startX, y));
                    } else {
                        mTiles.add(Tile.valueOf(TileType.GROUND, new Vector2(startX, y), new Vector2(endX, y)));
                    }

					chunk.clear();
				}
			}
			// The last, eventual chunk need a special case
			if(chunk.size() != 0) {

				float startX = chunk.get(0);
				float endX = chunk.get(chunk.size() -1);
                if(chunk.size() == 1) {
                    // Here's our singles with potential of mending in Y
                    mMendYTiles.addLast(new Vector2(startX, y));
                } else {
                    mTiles.add(Tile.valueOf(TileType.GROUND, new Vector2(startX, y), new Vector2(endX, y)));
                }

				chunk.clear();
			}
		}

        // Add all ys to hashmap with xs as key
        Map<Integer, List<Integer>> toMend = new HashMap<>();
        for(Vector2 v : mMendYTiles) {

            List<Integer> temp = toMend.get((int)v.x);
            if(temp == null) {
                temp = new ArrayList<>();
            }
            temp.add((int)v.y);

            toMend.put((int) v.x, temp);
        }

        // Mending the ys
        for(int x : toMend.keySet()) {
            ArrayList<Integer> ys = (ArrayList<Integer>) toMend.get(x);

            // Singles
            if(ys.size() == 1) {
                mTiles.add(Tile.valueOf(TileType.GROUND, new Vector2(x, ys.get(0)), new Vector2(x, ys.get(0))));
            } else {

                LinkedList<Integer> remaining = new LinkedList<>();
                Collections.sort(ys);

                for(int i = 0; i < ys.size(); i++) {

                    if(remaining.isEmpty()) {
                        remaining.addLast(ys.get(i));
                    } else {
                        int diff = ys.get(i) - remaining.peekLast();

                        if(diff > 1 || (i == ys.size() -1)) {
                            // Oh noes we got a gap
                            float startY = remaining.get(0);
                            float endY = remaining.get(remaining.size()-1);

                            mTiles.add(Tile.valueOf(TileType.GROUND, new Vector2(x, startY), new Vector2(x, endY + 1)));
                            remaining.clear();
                        } else {
                            remaining.addLast(ys.get(i));
                        }

                    }
                }
            }
        }

        // Makes a chunk out of water-tiles
        float mostLeftX = mLayer.getWidth(), mostRightX = 0, topY = 0, bottomY = mLayer.getHeight();

        if(!mWaterTiles.isEmpty()) {
            for(Vector2 w : mWaterTiles) {
                float x = w.x;
                float y = w.y;

                if(x < mostLeftX)
                    mostLeftX = x;
                if(x > mostRightX)
                    mostRightX = x;
                if(y < bottomY)
                    bottomY = y;
                if(y > topY)
                    topY = y;
            }

            // Adding the tile as one
            mTiles.add(Tile.valueOf(TileType.WATER, new Vector2(mostLeftX, topY+1), new Vector2(mostRightX, bottomY+1)));

            // Also a bottom
            EdgeShape waterBound = new EdgeShape();
            waterBound.set(new Vector2(mostLeftX, bottomY), new Vector2(mostRightX+1, bottomY));

            PhysicsBodyFactory pbf = new PhysicsBodyFactory.Builder(BodyType.StaticBody, waterBound).density(1f).
                    sensor(false).friction(2f).restitution(0).bullet(false).fixedRotation(true).userData(Tile.valueOf(TileType.DUMMY)).build();
        }

        // Also create bounds for the map
		new MapBounds();
		
		return true;
	}

    public List<Tile> getTiles() {
        return mTiles;
    }

    public List<Tile> getObjectTiles() {
        return mObjectTiles;
    }
	
	private final class MapBounds extends PassiveEntity {
		
		public MapBounds() {
			
			// Bottom
			EdgeShape mapBounds = new EdgeShape();
			mapBounds.set(new Vector2(0.0f, 0.0f), new Vector2(mLayer.getWidth(), 0.0f));
			
			PhysicsBodyFactory pbf = new PhysicsBodyFactory.Builder(BodyType.StaticBody, mapBounds).density(1f).
					sensor(false).friction(2f).restitution(0).bullet(false).fixedRotation(true).build();
			
			// Top (need to recreate everything since the bodyfactory disposes most of it)
            /* Open up the roof for now
			mapBounds = new EdgeShape();
			mapBounds.set(new Vector2(0.0f, mLayer.getHeight()), new Vector2(mLayer.getWidth(), mLayer.getHeight()));
			
			pbf = new PhysicsBodyFactory.Builder(BodyType.StaticBody, mapBounds).density(1f).
					sensor(false).friction(2f).restitution(0).bullet(false).fixedRotation(true).build();
			*/
			// Left
			mapBounds = new EdgeShape();
			mapBounds.set(new Vector2(0.0f, 0.0f), new Vector2(0.0f, mLayer.getHeight()));
			
			pbf = new PhysicsBodyFactory.Builder(BodyType.StaticBody, mapBounds).density(1f).
					sensor(false).friction(2f).restitution(0).bullet(false).fixedRotation(true).build();
			
			// Right
			mapBounds = new EdgeShape();
			mapBounds.set(new Vector2(mLayer.getWidth(), 0.0f),
					new Vector2(mLayer.getWidth(), mLayer.getHeight()));
			
			pbf = new PhysicsBodyFactory.Builder(BodyType.StaticBody, mapBounds).density(1f).
					sensor(false).friction(2f).restitution(0).bullet(false).fixedRotation(true).build();
		}
	}
}