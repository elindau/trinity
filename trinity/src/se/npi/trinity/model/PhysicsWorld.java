package se.npi.trinity.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.gushikustudios.box2d.controllers.B2Controller;

public enum PhysicsWorld {
	INSTANCE;

    private static Array<B2Controller> controllers;
	private static final World world;
	static { world = new World(new Vector2(0f, -19.8f), true); world.setContactListener(new CollisionsListener()); controllers = new Array<>();};
	
	public World world() {
		return world;
	}

    public Array<B2Controller> controllers() {
        return controllers;
    }

    public void addB2Controller(B2Controller toAdd) {
        controllers.add(toAdd);
    }
}
