package se.npi.trinity.model;

import com.gushikustudios.box2d.controllers.B2Controller;
import se.npi.trinity.model.entity.Player;
import se.npi.trinity.model.entity.Player.ShapeState;
import se.npi.trinity.model.entity.Player.SideSensor;
import se.npi.trinity.model.entity.Tile;
import se.npi.trinity.model.entity.Player.Footer;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

public class CollisionsListener implements ContactListener {

	private Body mBodyA, mBodyB;
	private Fixture mFixA, mFixB;
	
	@Override
	public void beginContact(Contact contact) {
		
		mFixA = contact.getFixtureA();
		mFixB = contact.getFixtureB();
		
		mBodyA = contact.getFixtureA().getBody();
		mBodyB = contact.getFixtureB().getBody();

        if(mFixA.getUserData() instanceof B2Controller) {
            B2Controller b2c = (B2Controller) mFixA.getUserData();
            if(mBodyB.getUserData() instanceof Player) {
                Player p = (Player) mBodyB.getUserData();

                if(p.getCurrentShapeState() == ShapeState.LYING) {
                    // Adding to the controller
                    b2c.addBody(mBodyB);
                } else {
                    // .. else adding to scheduled services
                    if(!WorldModel.scheduldedForSubLife.contains(p, true)) {
                        WorldModel.scheduldedForSubLife.add(p);
                    }
                }

            }
        }

        if(mFixB.getUserData() instanceof B2Controller) {
            B2Controller b2c = (B2Controller) mFixB.getUserData();
            if(mBodyA.getUserData() instanceof Player) {
                Player p = (Player) mBodyA.getUserData();

                if(p.getCurrentShapeState() == ShapeState.LYING) {
                    // Adding to the controller
                    b2c.addBody(mBodyA);
                } else {
                    // .. else adding to scheduled services
                    if(!WorldModel.scheduldedForSubLife.contains(p, true)) {
                        WorldModel.scheduldedForSubLife.add(p);
                    }
                }

            }
        }

        // Checkpoints
        if(mBodyA.getUserData() instanceof Player) {
            if(mBodyB.getUserData() instanceof Tile) {
                Tile t = (Tile) mBodyB.getUserData();
                Player p = (Player) mBodyA.getUserData();
                if(t.type == Tile.TileType.CHECKPOINT) {
                    t.enabled = true;
                    p.saveAt(t.readPosition);
                }
            }
        }

        if(mBodyB.getUserData() instanceof Player) {
            if(mBodyA.getUserData() instanceof Tile) {
                Tile t = (Tile) mBodyA.getUserData();
                Player p = (Player) mBodyB.getUserData();
                if(t.type == Tile.TileType.CHECKPOINT) {
                    t.enabled = true;
                    p.saveAt(t.readPosition);
                }
            }
        }
		
		// Special case for footer
		if(mFixA.getUserData() instanceof Footer) {
			if(mBodyB.getUserData() instanceof Tile) {
				Footer f = (Footer) mFixA.getUserData();
				f.getPlayer().setGrounded(true);
			}
		}
			
		if(mFixB.getUserData() instanceof Footer) {
			if(mBodyA.getUserData() instanceof Tile) {
				Footer f = (Footer) mFixB.getUserData();
				f.getPlayer().setGrounded(true);
			}
		}
		
		// For the sensors
		if(mFixA.getUserData() instanceof SideSensor) {
			if(mBodyB.getUserData() instanceof Tile) {
				SideSensor ss = (SideSensor) mFixA.getUserData();
				ss.getPlayer().setShapeChangeAvailability(ss.getShapeState(), false);
			}
		}
		
		if(mFixB.getUserData() instanceof SideSensor) {
			if(mBodyA.getUserData() instanceof Tile) {
				SideSensor ss = (SideSensor) mFixB.getUserData();
				ss.getPlayer().setShapeChangeAvailability(ss.getShapeState(), false);
			}
		}
		
		collisionHandlerBegin(mBodyA, mBodyB);
	}
	
	private void collisionHandlerBegin(Body a, Body b) {

	}

	@Override
	public void endContact(Contact contact) {
		mFixA = contact.getFixtureA();
		mFixB = contact.getFixtureB();
		
		mBodyA = contact.getFixtureA().getBody();
		mBodyB = contact.getFixtureB().getBody();

        if(mFixA.getUserData() instanceof B2Controller) {
            B2Controller b2c = (B2Controller) mFixA.getUserData();
            b2c.removeBody(mBodyB);
        }

        if(mFixB.getUserData() instanceof B2Controller) {
            B2Controller b2c = (B2Controller) mFixB.getUserData();
            b2c.removeBody(mBodyA);
        }
		
		// Special case for footer
		if(mFixA.getUserData() instanceof Footer) {
			if(mBodyB.getUserData() instanceof Tile) {
				Footer f = (Footer) mFixA.getUserData();
				f.getPlayer().setGrounded(false);
			}
		}
			
		if(mFixB.getUserData() instanceof Footer) {
			if(mBodyA.getUserData() instanceof Tile) {
				Footer f = (Footer) mFixB.getUserData();
				f.getPlayer().setGrounded(false);
			}
		}
		
		if(mFixA.getUserData() instanceof SideSensor) {
			if(mBodyB.getUserData() instanceof Tile) {
				SideSensor ss = (SideSensor) mFixA.getUserData();
				ss.getPlayer().setShapeChangeAvailability(ss.getShapeState(), true);
			}
		}
		
		if(mFixB.getUserData() instanceof SideSensor) {
			if(mBodyA.getUserData() instanceof Tile) {
				SideSensor ss = (SideSensor) mFixB.getUserData();
				ss.getPlayer().setShapeChangeAvailability(ss.getShapeState(), true);
			}
		}
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {

	}

}
