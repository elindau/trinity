package se.npi.trinity.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;

public class WaterRayHandlerCallback implements RayCastCallback {

    public boolean hit;
    public Vector2 point;

    public WaterRayHandlerCallback() {
        hit = false;
    }

    @Override
    public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
        hit = true;
        this.point = point;
        return fraction;
    }

}
