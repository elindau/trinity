package se.npi.trinity.model;

import com.gushikustudios.box2d.controllers.B2Controller;
import se.npi.trinity.model.entity.MovingEntity;
import se.npi.trinity.model.entity.Player;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class WorldModel {

    // Schedulded things
    public static Array<MovingEntity> scheduldedForSubLife = new Array<>();

	// private World mPhysicsWorld;
	private Array<Body> mBodies;
	
	private Player mPlayer;
	
	public WorldModel() {
		// mPhysicsWorld = new World(new Vector2(0f, 0f), true);
		mBodies = new Array<Body>();
		
		// Adding the player by code, aoch
		mPlayer = new Player(new Vector2(5f, 15f));
	}
	
	public void update(float delta) {

        if(scheduldedForSubLife.size != 0) {
            for(MovingEntity entity : scheduldedForSubLife) {
                entity.subLife();
            }
            scheduldedForSubLife.clear();
        }

        // Step the controllers
        Array<B2Controller> ctrls = PhysicsWorld.INSTANCE.controllers();
        for(int i = 0; i < ctrls.size; i++) {
            ctrls.get(i).step(1/60f);
        }

        // Step the box2d world
		PhysicsWorld.INSTANCE.world().step(1/60f, 6, 2);
		
		// Update
		PhysicsWorld.INSTANCE.world().getBodies(mBodies);
		for(Body b : mBodies) {
			if(b.getUserData() instanceof MovingEntity) {
                MovingEntity entity = ((MovingEntity)b.getUserData());
                if(entity.isAlive()) {
                    entity.update(delta);
                }
			}
		}
	}
	
	public Player getPlayer() {
		return mPlayer;
	}
}
