package se.npi.trinity.model.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public abstract class MovingEntity implements IEntity {

    public static final float DEFAULT_LIVES = 1f;
    public static final float LIFE_STEP = 0.5f;

	private Vector2 mSize;
	private Body mBody;

    private float mLives;
    private final float mMaxLives;
	
	public MovingEntity(Vector2 size) {
		this(size, DEFAULT_LIVES);
	}

    public MovingEntity(Vector2 size, float lives) {
        mSize = size;
        lives = lives / 2;
        mLives = lives;
        mMaxLives = lives;
    }
	
	public abstract void update(float delta);
    public abstract void spawn(Vector2 spawnPoint);

    public void kill() {
        mLives = 0;
    }

    public void addLife(float amount) {
        mLives += amount;
        if(mLives > mMaxLives)
            mLives = mMaxLives;
    }

    public void subLife() {
        mLives -= LIFE_STEP;
        if(mLives < 0)
            mLives = 0;
    }

    public float getLife() {
        return mLives;
    }

    public boolean isAlive() {
        return mLives > 0 ? true : false;
    }
	
	public void setVelocity(float x, float y) {
		mBody.setLinearVelocity(x, y);
	}
	
	public void setVelocity(Vector2 newVel) {
		setVelocity(newVel.x, newVel.y);
	}
	
	public Vector2 getVelocity() {
		return mBody.getLinearVelocity();
	}
	
	@Override
	public Body getBody() {
		return mBody;
	}
	
	public void setBody(Body body) {
		mBody = body;
	}
	
	@Override
	public Vector2 getPosition() {
		return mBody.getPosition();
	}

	@Override
	public Vector2 getSize() {
		return mSize;
	}
	
	protected void setSize(Vector2 newSize) {
		setSize(newSize.x, newSize.y);
	}
	
	protected void setSize(float x, float y) {
		mSize = new Vector2(x, y);
		//mSize.x = x;
		//mSize.y = y;
	}
	
	public void setPosition(float x, float y) {
		mBody.setTransform(x, y, 0);
	}
	
	public void setPosition(Vector2 newPos) {
		setPosition(newPos.x, newPos.y);
	}
	
	// For recursive use in w/e class extending this
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		return mBody.hashCode();
	}
	
	@Override
	public String toString() {
		return "Abstract class MovingEntity, intended for characters and suchlike";
	}
}
