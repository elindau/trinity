package se.npi.trinity.model.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class PassiveEntity implements IEntity {
	
	@Override
	public Vector2 getPosition() {
		return null;
	}

	@Override
	public Vector2 getSize() {
		return null;
	}

	@Override
	public Body getBody() {
		return null;
	}
}
