package se.npi.trinity.model.entity;

import com.badlogic.gdx.physics.box2d.*;
import com.gushikustudios.box2d.controllers.B2BuoyancyController;
import se.npi.trinity.model.PhysicsBodyFactory;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import se.npi.trinity.model.PhysicsWorld;

/**
 * Trying to keep Tile as an immutable object
 * 
 * @author Emil
 * 
 */
public class Tile extends PassiveEntity {

	private static final Vector2 TILE_SIZE = new Vector2(1f, 1f);

    public Vector2 readPosition = new Vector2();
    public boolean enabled = false;
    public TileType type;
	
	private Body mBody;

	private Tile(TileType type, Vector2 pos, Vector2 size) {

        PolygonShape poly;
        PhysicsBodyFactory pbf;
        this.type = type;

        switch (type) {

            case DUMMY:
                break;

            case CHECKPOINT:
                // Creating the shape for the body
                poly = new PolygonShape();
                poly.setAsBox(size.x / 2, size.y / 2);

                // Using builder here cause ALOT of parameters
                readPosition = new Vector2(pos.x, pos.y);
                pbf = new PhysicsBodyFactory.Builder(BodyType.StaticBody, poly).density(0f).
                        sensor(true).friction(0f).restitution(0f).bullet(false).fixedRotation(true).userData(this).build();


                // Get the generated body from the builder
                mBody = pbf.getBody();
                // Set spawn, done
                mBody.setTransform(pos.x + size.x / 2, pos.y + size.y / 2, 0);
                break;

		    case GROUND:
			    // Creating the shape for the body
			    poly = new PolygonShape();
			    poly.setAsBox(size.x / 2, size.y / 2);
			
			    // Using builder here cause ALOT of parameters
			    pbf = new PhysicsBodyFactory.Builder(BodyType.StaticBody, poly).density(1f).
					sensor(false).friction(0f).restitution(0f).bullet(false).fixedRotation(true).userData(this).build();
			
			    // Get the generated body from the builder
			    mBody = pbf.getBody();
			    // Set spawn, done
			    mBody.setTransform(pos.x + size.x / 2, pos.y + size.y / 2, 0);
			    break;

            case WATER:
                // Also adding the buoyancy controller
                B2BuoyancyController b2c = new B2BuoyancyController(B2BuoyancyController.DEFAULT_SURFACE_NORMAL, B2BuoyancyController.DEFAULT_FLUID_VELOCITY,
                        PhysicsWorld.INSTANCE.world().getGravity(), pos.y, 7f, B2BuoyancyController.DEFAULT_LINEAR_DRAG,
                        B2BuoyancyController.DEFAULT_ANGULAR_DRAG);
                PhysicsWorld.INSTANCE.addB2Controller(b2c);

                // Creating the shape for the body
                poly = new PolygonShape();
                poly.setAsBox(size.x / 2, size.y / 2);

                BodyDef bodyDef = new BodyDef();
                bodyDef.type = BodyType.StaticBody;
                bodyDef.allowSleep = false;

                mBody = PhysicsWorld.INSTANCE.world().createBody(bodyDef);
                FixtureDef fixtureDef = new FixtureDef();
                fixtureDef.density = 0f;
                fixtureDef.isSensor = true;
                fixtureDef.shape = poly;
                fixtureDef.friction = 0f;
                fixtureDef.restitution = 0f;

                Fixture fixture = mBody.createFixture(fixtureDef);
                fixture.setUserData(b2c);
                mBody.setBullet(false);
                mBody.setFixedRotation(true);

                // Set spawn, done
                mBody.setTransform(pos.x + size.x / 2, pos.y - size.y / 2, 0);

                break;

		    default:
			break;
		}
	}

    public static Tile valueOf(TileType type) {
        return new Tile(type, Vector2.Zero, TILE_SIZE);
    }

	public static Tile valueOf(TileType type, Vector2 pos) {
		return new Tile(type, pos, TILE_SIZE);
	}
	
	public static Tile valueOf(TileType type, Vector2 startPos, Vector2 endPos) {
		Vector2 size = new Vector2();
		size.x = (endPos.x - startPos.x) + TILE_SIZE.x;

        if(type == TileType.WATER)
		    size.y = (startPos.y - endPos.y) + TILE_SIZE.y;
        else
            size.y = (endPos.y - startPos.y) + TILE_SIZE.y;

		return new Tile(type, startPos, size);
	}

	public enum TileType {
		GROUND, WATER, DUMMY, CHECKPOINT;
	}
}
