package se.npi.trinity.model.entity;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.physics.box2d.*;
import se.npi.trinity.controller.WorldController.MapKeys;
import se.npi.trinity.model.PhysicsBodyFactory;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import se.npi.trinity.model.PhysicsWorld;
import se.npi.trinity.model.WaterRayHandlerCallback;

public class Player extends MovingEntity {
	
	// Just remembering the size
	// 12x28
	public static final Vector2 PLAYER_SIZE = new Vector2(.75f, 1.75f);
	private static final int NO_OF_SHAPES = 3;

    private static final float LIVES = 6f;
	
	// Jumping
	private boolean mIsGrounded = true;
	private boolean mJumpEnabled = true;
	private static final float JUMP_IMPULSE_STANDING = 10f;
	private static final float JUMP_IMPULSE_LAYING = 6f;
	private static final float JUMP_IMPULSE_CUBE = 0f;
	private float mJumpImpulse = JUMP_IMPULSE_STANDING;
	
	// Moving
	private MoveState mCurrentState = MoveState.STOP;	
	public enum MoveState { LEFT, RIGHT, STOP; }
	
	// ShapeChanging
	private boolean mCanChange = true;
	private final Fixture[] mShapeList = new Fixture[NO_OF_SHAPES];
	private int mCurrentIndexShape;
	public enum ShapeState { STANDING, LYING, CUBE; }
	
	// Map for iterate trough shapes
	private static final Map<Integer, ShapeState> mShapeStates = new HashMap<>();
	static {
		mShapeStates.put(0, ShapeState.STANDING);
		mShapeStates.put(1, ShapeState.LYING);
		mShapeStates.put(2, ShapeState.CUBE);
	}
	
	// Map for checking availability of shapechange
	private static final Map<ShapeState, Boolean> mShapeChangeAvailability = new HashMap<>();
	static {
		mShapeChangeAvailability.put(ShapeState.STANDING, true);
		mShapeChangeAvailability.put(ShapeState.LYING, true);
		mShapeChangeAvailability.put(ShapeState.CUBE, true);
	}

    private Vector2 mSaveState;
	
	/**
	 * UserData class for the foot-sensor
	 * @author Emil
	 *
	 */
	public final class Footer {
		private final Player mPlayer;
		public Footer(Player player) { mPlayer = player; }
		public Player getPlayer() { return mPlayer; }
	}
	
	public final class SideSensor {
		private final Player mPlayer;
		private final ShapeState mType;
		public SideSensor(Player player, ShapeState type) {
			mPlayer = player;
			mType = type;
		}
		public Player getPlayer() { return mPlayer; }
		public ShapeState getShapeState() { return mType; }
	}
	
	public Player(Vector2 spawnPoint) {
		super(PLAYER_SIZE, LIVES);
		
		// Creating the shape for the body
		PolygonShape poly = new PolygonShape();
		poly.setAsBox(PLAYER_SIZE.x / 2, PLAYER_SIZE.y / 2);
		
		// Using builder here cause ALOT of parameters
		PhysicsBodyFactory pbf = new PhysicsBodyFactory.Builder(BodyType.DynamicBody, poly).density(1f).
				sensor(false).friction(0f).bullet(true).restitution(.1f).fixedRotation(true).userData(this).build();
		
		// Get the generated body from the builder
		setBody(pbf.getBody());
		
		// For the shapes
		mCurrentIndexShape = 0;
		mShapeList[mCurrentIndexShape] = pbf.getFixture();
		
		// Adding the many, clumsy fixtures
		addFixtures();

        // spawn
        spawn(spawnPoint);
    }

    @Override
    public void spawn(Vector2 spawnPoint) {
        // Resetting the index
        mCurrentIndexShape = 0;

        // Set spawn, done
        setPosition(spawnPoint.x + PLAYER_SIZE.x / 2, spawnPoint.y + PLAYER_SIZE.y / 2);
    }

    @Override
    public void subLife() {
        super.subLife();

        if(mSaveState != null) {
            spawn(mSaveState);
        } else {
            spawn(new Vector2(5f, 10f));
        }

    }

	
	/**
	 * Adding the different fixtures, shapes to change to and
	 * sensors
	 */
	private void addFixtures() {

        Vector2 centerPos = getBody().getLocalCenter();

		// Adding, as well, a foot sensor for jumping - a tiny bit tedious
		PolygonShape poly = new PolygonShape();
		poly.setAsBox((PLAYER_SIZE.x / 2) * 0.95f, (PLAYER_SIZE.y / 2) * 0.25f, new Vector2(0f, -(PLAYER_SIZE.y / 2)), 0);
		FixtureDef footer = new FixtureDef();
		footer.density = 0f;
		footer.shape = poly;
		footer.isSensor = true;
		getBody().createFixture(footer).setUserData(new Footer(this));
		
		// Sensor for standing one
		poly.setAsBox((PLAYER_SIZE.x / 2) * 0.95f, (PLAYER_SIZE.y / 2) * 0.25f, new Vector2(0f, (PLAYER_SIZE.y / 2)), 0);
		FixtureDef topSensor = new FixtureDef();
		topSensor.density = 0f;
		topSensor.shape = poly;
		topSensor.isSensor = true;
		getBody().createFixture(topSensor).setUserData(new SideSensor(this, ShapeState.STANDING));
		
				
		// The diffrent shapes our player can take
		// The lying rectangle
		poly.setAsBox(PLAYER_SIZE.y / 2, PLAYER_SIZE.x / 2);
		FixtureDef sideRect = new FixtureDef();
		sideRect.density = 1f;
		sideRect.shape = poly;
		sideRect.isSensor = true;
				
		// Adding the fixtures to the list
		Fixture f1 = getBody().createFixture(sideRect);
		f1.setUserData(this);
		mCurrentIndexShape = 1;
		mShapeList[mCurrentIndexShape] = f1;
		
		// Sensor for lying one
		poly.setAsBox((PLAYER_SIZE.y / 2) * 0.25f, (PLAYER_SIZE.x / 2) * 0.9f, new Vector2(-(PLAYER_SIZE.y / 2), 0f), 0);
		FixtureDef sideSensor = new FixtureDef();
		sideSensor.density = 0f;
		sideSensor.shape = poly;
		sideSensor.isSensor = true;
		getBody().createFixture(sideSensor).setUserData(new SideSensor(this, ShapeState.LYING));
		
		poly.setAsBox((PLAYER_SIZE.y / 2) * 0.25f, (PLAYER_SIZE.x / 2) * 0.9f, new Vector2(PLAYER_SIZE.y / 2, 0f), 0);
		sideSensor.shape = poly;
		getBody().createFixture(sideSensor).setUserData(new SideSensor(this, ShapeState.LYING));
		
				
		// The cube - doesn't need any sensors
		poly.setAsBox(PLAYER_SIZE.x / 2, PLAYER_SIZE.x / 2);
		FixtureDef cube = new FixtureDef();
		cube.density = 1f;
		cube.shape = poly;
		cube.isSensor = true;
				
		Fixture f2 = getBody().createFixture(cube);
		f2.setUserData(this);
		mCurrentIndexShape = 2;
		mShapeList[mCurrentIndexShape] = f2;
				
		poly.dispose();
	}
	
	public void tryChangeShape(boolean next) {
		if(mCanChange) {
			mCanChange = false;

            if(next) {
                subLife();
            } else {
                addLife(0.5f);
            }
			// Here is the actual trying of the shapechange
			stepShapeIndex(next);
            MassData md = new MassData();

			// We need to change the access of the size
			switch(getCurrentShapeState()) {
			case CUBE:
				setSize(PLAYER_SIZE.x, PLAYER_SIZE.x);
				mJumpImpulse = JUMP_IMPULSE_CUBE;
                getBody().setGravityScale(5);
				break;
			case LYING:
				setSize(PLAYER_SIZE.y, PLAYER_SIZE.x);
				mJumpImpulse = JUMP_IMPULSE_LAYING;
                getBody().setGravityScale(1);
				break;
			case STANDING:
				setSize(PLAYER_SIZE.x, PLAYER_SIZE.y);
				mJumpImpulse = JUMP_IMPULSE_STANDING;
                getBody().setGravityScale(1);
				break;
			}
		}
	}
	
	/**
	 * Try and change shape
	 * @param next change to next or previous shape
	 */
	private void stepShapeIndex(boolean next) {
		
		int previousShape = mCurrentIndexShape;
		
		if(next) {
			mCurrentIndexShape++;
		} else {
			mCurrentIndexShape--;
		}
			
		if(mCurrentIndexShape > NO_OF_SHAPES - 1)
			mCurrentIndexShape = 0;
		if(mCurrentIndexShape < 0)
			mCurrentIndexShape = NO_OF_SHAPES - 1;
		
		// Is the change doable?
		if(getShapeChangeAvailability(getCurrentShapeState())) {
			// We're in the clear and can change shape
			mShapeList[previousShape].setSensor(true);
			mShapeList[mCurrentIndexShape].setSensor(false);
		} else {
			mCurrentIndexShape = previousShape;
		}
	}
	
	// Set if we can change to next/previous shape
	public void setShapeChangeAvailability(ShapeState type, boolean val) {
		System.out.println(type + " : " + val);
		mShapeChangeAvailability.get(mShapeChangeAvailability.put(type, val));
	}
	
	private boolean getShapeChangeAvailability(ShapeState type) {
		return mShapeChangeAvailability.get(type);
	}
	
	// For releasing
	public void reEnableShapeChange() {
		mCanChange = true;
	}
	
	// Get the players current shape
	public ShapeState getCurrentShapeState() {
		return mShapeStates.get(mCurrentIndexShape);
	}
	
	
	public void standStill() {
		mCurrentState = MoveState.STOP;
	}
	
	public void moveLeft() {
		if(mCurrentState == MoveState.RIGHT) {
			getBody().setLinearVelocity(0f, getVelocity().y);
		}
		mCurrentState = MoveState.LEFT;	
	}
	
	public void moveRight() {
		if(mCurrentState == MoveState.LEFT) {
			getBody().setLinearVelocity(0f, getVelocity().y);
		} 
		mCurrentState = MoveState.RIGHT;
	}
	
	public void jump() {
		if(mIsGrounded && mJumpEnabled) {
			float impulse = getBody().getMass() * mJumpImpulse;
		    getBody().applyLinearImpulse(new Vector2(0f, impulse), getBody().getWorldCenter(), true);
		    // mIsGrounded = false;
		    mJumpEnabled = false;
		}
	}
	
	public void reEnableJump() {
		mJumpEnabled = true;
	}
	
	public void setGrounded(boolean val) {
		mIsGrounded = val;
	}

    public void saveAt(Vector2 pos) {
        if(mSaveState == null)
            mSaveState = new Vector2();
        if(mSaveState.equals(pos))
            return;

        mSaveState = pos.cpy();
    }

	@Override
	public void update(float delta) {
		
		float desiredVel = 0f;
		switch(mCurrentState) {
		case LEFT:
			// getBody().setLinearVelocity(getVelocity().x - 5f * delta, getVelocity().y);
			desiredVel = Math.max(getVelocity().x - 0.2f, -7.0f);
			break;
		case RIGHT:
			// getBody().setLinearVelocity(getVelocity().x + 5f * delta, getVelocity().y);
			desiredVel = Math.min(getVelocity().x + 0.2f, 7.0f);
			break;
		case STOP:
			getBody().setLinearVelocity(0f, getVelocity().y);
			// desiredVel = getVelocity().x * 0.7f;
			break;
		}
		float velChange = desiredVel - getVelocity().x;
		float impulse = getBody().getMass() * velChange;
		getBody().applyLinearImpulse(new Vector2(impulse, 0f), getBody().getWorldCenter(), true);
		
	}
}
