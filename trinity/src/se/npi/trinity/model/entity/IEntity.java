package se.npi.trinity.model.entity;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public interface IEntity {

	public Body getBody();
	public Vector2 getPosition();

	
	public Vector2 getSize();
}
