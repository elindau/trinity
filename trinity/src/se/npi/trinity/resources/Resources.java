package se.npi.trinity.resources;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import se.npi.trinity.model.TiledMapHelper;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;


public enum Resources {
	INSTANCE;
	
	private static AssetManager mManager;
	static{ mManager = new AssetManager(); };
	
	public TextureAtlas playerAtlas;
    public TextureAtlas healthAtlas;
    public TextureAtlas checkpointAtlas;
	public TiledMapHelper mapHelper;
    public Texture water;
    public Texture waterDisplacement;
	public Texture background;
    public BitmapFont font;

	private TiledMap map;

	public AssetManager getAssetManager() {
		return mManager;
	}
	
	public void loadGameResources() {
		// Gfx
		mManager.load("gfx/playerPack.atlas", TextureAtlas.class);
        mManager.load("gfx/heart.pack", TextureAtlas.class);
        mManager.load("gfx/checkpoint.pack", TextureAtlas.class);
		mManager.load("gfx/water/water.png", Texture.class);
        mManager.load("gfx/water/waterdisplacement.png", Texture.class);
        mManager.load("gfx/bg.png", Texture.class);

		// Tiled
		mManager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
		mManager.load("maps/level3.tmx", TiledMap.class);

        mManager.load("data/fonts/hud.fnt", BitmapFont.class);
	}
	
	public boolean setResourceInstances() {
		playerAtlas = mManager.get("gfx/playerPack.atlas", TextureAtlas.class);
		map = mManager.get("maps/level3.tmx", TiledMap.class);
        water = mManager.get("gfx/water/water.png", Texture.class);
        waterDisplacement = mManager.get("gfx/water/waterdisplacement.png", Texture.class);
        background = mManager.get("gfx/bg.png", Texture.class);
        healthAtlas = mManager.get("gfx/heart.pack", TextureAtlas.class);
        checkpointAtlas = mManager.get("gfx/checkpoint.pack", TextureAtlas.class);

		mapHelper = new TiledMapHelper(map);

        font = mManager.get("data/fonts/hud.fnt", BitmapFont.class);
		
		return true;
	}
}
