package se.npi.trinity.view;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;
import se.npi.trinity.Camera;
import se.npi.trinity.model.WorldModel;
import se.npi.trinity.model.entity.Player;
import se.npi.trinity.model.entity.Player.ShapeState;
import se.npi.trinity.resources.Resources;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class PlayerRenderer extends ARenderer {
	
	private static final String CUBE = "player_cube";
	private static final String LYING = "player_laing";
	private static final String STANDING = "player_standing";
	private static final String MOUSTACHE = "player_moustache";
    public static final int FULL_HEART = 0;
    public static final int HALF_HEART = 1;
    public static final int EMPTY_HEART = 2;
	
	// private Sprite mPlayer;
	private Player mWorldPlayer;
	
	private Sprite mPowerMoustache;
	
	private static final Map<ShapeState, Sprite> mPlayer = new HashMap<ShapeState, Sprite>();
	static {
		Sprite s = Resources.INSTANCE.playerAtlas.createSprite(STANDING);
		s.setSize(Player.PLAYER_SIZE.x, Player.PLAYER_SIZE.y);
		mPlayer.put(ShapeState.STANDING, s);
		
		Sprite s1 = Resources.INSTANCE.playerAtlas.createSprite(LYING);
		s1.setSize(Player.PLAYER_SIZE.y, Player.PLAYER_SIZE.x);
		mPlayer.put(ShapeState.LYING, s1);
		
		Sprite s2 = Resources.INSTANCE.playerAtlas.createSprite(CUBE);
		s2.setSize(Player.PLAYER_SIZE.x, Player.PLAYER_SIZE.x);
		mPlayer.put(ShapeState.CUBE, s2);
	}

    private static final Map<Integer, Array<Sprite>> mLives = new HashMap<>();
    static {
        float offset = 1.1f;
        float currentX = 1f;
        for(int i = 0; i < 3; i++) {
            Array<Sprite> sprites = new Array<>();
            sprites.add(Resources.INSTANCE.healthAtlas.createSprite("heart", 0));
            sprites.add(Resources.INSTANCE.healthAtlas.createSprite("heart", 1));
            sprites.add(Resources.INSTANCE.healthAtlas.createSprite("heart", 2));
            for (Sprite sprite : sprites) {
                sprite.setSize(1f, 1f);
                sprite.setPosition(currentX, Camera.CAMERA_HEIGHT - 1f - 0.5f);
            }
            currentX += offset;
            mLives.put(i, sprites);
        }
    }
	
	public PlayerRenderer(WorldModel model, Camera cam, SpriteBatch batch) {
		super(model, cam, batch);
		
		mWorldPlayer = model.getPlayer();
		mPowerMoustache = Resources.INSTANCE.playerAtlas.createSprite(MOUSTACHE);
		mPowerMoustache.setSize(1f, .75f);
	}

    private int getCurrentHeart(int heart) {
        float currentLife = mWorldPlayer.getLife();
        switch (heart) {
            case 0:
                if(currentLife == 0)
                    return EMPTY_HEART;
                if(currentLife == 0.5)
                    return HALF_HEART;
                if(currentLife > 0.5)
                    return FULL_HEART;
                break;
            case 1:
                if(currentLife == 1)
                    return EMPTY_HEART;
                if(currentLife == 1.5)
                    return HALF_HEART;
                if(currentLife > 1.5)
                    return FULL_HEART;
                break;
            case 2:
                if(currentLife == 2)
                    return EMPTY_HEART;
                if(currentLife == 2.5)
                    return HALF_HEART;
                if(currentLife > 2.5)
                    return FULL_HEART;
                break;
        }
        return EMPTY_HEART;
    }
	
	@Override
	public void draw(float delta) {
		mPlayer.get(mWorldPlayer.getCurrentShapeState()).setPosition(mWorldPlayer.getPosition().x - mWorldPlayer.getSize().x / 2, mWorldPlayer.getPosition().y - mWorldPlayer.getSize().y / 2);
		mPlayer.get(mWorldPlayer.getCurrentShapeState()).draw(smSpriteBatch);
		
		mPowerMoustache.setPosition(mWorldPlayer.getPosition().x - 1f / 2, mWorldPlayer.getPosition().y - .75f / 2);
		mPowerMoustache.draw(smSpriteBatch);

        mLives.get(0).get(getCurrentHeart(0)).draw(smSpriteBatch);
        mLives.get(1).get(getCurrentHeart(1)).draw(smSpriteBatch);
        mLives.get(2).get(getCurrentHeart(2)).draw(smSpriteBatch);
	}

    @Override
    public void dispose() {

    }
}
