package se.npi.trinity.view;

import se.npi.trinity.Camera;
import se.npi.trinity.controller.WorldController;
import se.npi.trinity.model.PhysicsWorld;
import se.npi.trinity.model.TiledMapHelper;
import se.npi.trinity.model.WorldModel;
import se.npi.trinity.resources.Resources;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

public class WorldView {

	private WorldModel mWorldModel;
	private Camera mCamera;
	private SpriteBatch mSpriteBatch;
	
	private Box2DDebugRenderer mDebugRenderer;
	
	private OrthogonalTiledMapRenderer mMapRenderer;
	
	private PlayerRenderer mPlayerRenderer;
    private WaterRenderer mWaterRenderer;
    private ObjectRenderer mObjectRenderer;
    private BackgroundRenderer mBackgroundRenderer;
    private Text mText;
    private Light mLight;
	
	public WorldView(WorldModel worldModel, Camera camera) {
		mWorldModel = worldModel;
		mCamera = camera;
		mSpriteBatch = new SpriteBatch();
		mDebugRenderer = new Box2DDebugRenderer();
		
		mPlayerRenderer = new PlayerRenderer(worldModel, camera, mSpriteBatch);
        mWaterRenderer = new WaterRenderer(worldModel, camera, mSpriteBatch);
        mObjectRenderer = new ObjectRenderer(worldModel, camera, mSpriteBatch);
        mBackgroundRenderer = new BackgroundRenderer(worldModel, camera, mSpriteBatch);
        mText = new Text("", worldModel, camera, mSpriteBatch);
        mLight = new Light(worldModel, camera, mSpriteBatch);

		// Rendering the map
		// Scale 1unit = 16px
		float unitScale = 1 / 21f;
		mMapRenderer = new OrthogonalTiledMapRenderer(Resources.INSTANCE.mapHelper.getMap(), unitScale);
	}
	
	public void draw(float delta) {
        int[] tiledLayers = { TiledMapHelper.LAYER_TILES, TiledMapHelper.LAYER_FOREGROUND };
        // int[] fgLayers = { TiledMapHelper.LAYER_FOREGROUND };
        int[] bgLayers = { TiledMapHelper.LAYER_BACKGROUND };

        mLight.draw(delta);

        // The actual bg
		mSpriteBatch.begin();
		mSpriteBatch.setProjectionMatrix(mCamera.combined);
        // mBackground.draw(mSpriteBatch);
        mBackgroundRenderer.draw(delta);
		mSpriteBatch.end();

        // Text
        mText.draw(delta);

        // Tiled bg
        mMapRenderer.setView(mCamera);
        mMapRenderer.render(bgLayers);

        // Everything else
        mSpriteBatch.begin();
        mSpriteBatch.setProjectionMatrix(mCamera.combined);
        mObjectRenderer.draw(delta);
        mPlayerRenderer.draw(delta);

		mSpriteBatch.end();

		
		//mSpriteBatch.begin();

        // Only render the actual layer

        // Foreground and tiles
        mMapRenderer.render(tiledLayers);
        mWaterRenderer.draw(delta);


		
		if(WorldController.DEBUG)
			mDebugRenderer.render(PhysicsWorld.INSTANCE.world(), mCamera.combined);		
		
		//mSpriteBatch.end();
	}

    public void dispose() {

    }
}
