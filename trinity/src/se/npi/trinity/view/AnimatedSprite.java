package se.npi.trinity.view;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimatedSprite extends Sprite
{
    protected final TextureAtlas mTextureAtlas;

    protected AtlasRegion mFrame[];
    protected int mCurFrame;

    protected float mAnimationRateInSeconds;
    protected float mFrameTimeCounter;
    protected boolean mIsPlaying;
    protected boolean mIsFinished;
    protected boolean mLoop;

    protected boolean mFlipX;
    protected boolean mFlipY;

    public AnimatedSprite(final TextureAtlas textureAtlas, final String regionName, final int frameCount)
    {
        super( textureAtlas.findRegion( regionName, 0 ) );
        mTextureAtlas = textureAtlas;

        mAnimationRateInSeconds = 1.0f;
        mIsPlaying = false;
        mIsFinished = false;
        mLoop = true;

        mFlipX = false;
        mFlipY = false;

        mFrame = new AtlasRegion[ frameCount ];
        for( int ii=0; ii<frameCount; ++ii )
        {
            mFrame[ii] = mTextureAtlas.findRegion( regionName, ii );
        }

        mCurFrame = 0;
    }

    public void setAnimationRate(final int fps)
    {
        mAnimationRateInSeconds = 1.0f / (float)fps;
    }

    public void loop( boolean doLoop )
    {
        mLoop = doLoop;
    }

    public final boolean looping()
    {
        return mLoop;
    }

    public void freeze(int frame) {
        mCurFrame = frame;
        pause();
    }

    public void restart()
    {
        mIsPlaying = true;
        mFrameTimeCounter = 0;
        mCurFrame = 0;
        mIsFinished = false;
    }

    public void play()
    {
        mIsPlaying = true;
        mFrameTimeCounter = 0;
        mIsFinished = false;
    }

    public void pause()
    {
        mIsPlaying = false;
        mFrameTimeCounter = 0;
        mIsFinished = false;
    }

    public void stop()
    {
        pause();
        mCurFrame = mFrame.length - 1;
        mIsFinished = true;
    }

    public boolean isFinished() {
        return mIsFinished;
    }

    public void update(final float secondsElapsed)
    {
        if( mIsPlaying )
        {
            mFrameTimeCounter += secondsElapsed;
            if(mFrameTimeCounter > mAnimationRateInSeconds)
            {
                mFrameTimeCounter = 0;
                ++mCurFrame;

                // Back to first
                if( mCurFrame >= mFrame.length )
                {
                    // If not looping, stop
                    if( !mLoop )
                    {
                        stop();
                    }
                    // Other wise just reset
                    else
                    {
                        mCurFrame = 0;
                    }
                }
            }
        }

        setRegion( mFrame[ mCurFrame ] );
    }

    public void setRegion (TextureRegion region)
    {
        super.setRegion( region );
        super.flip( mFlipX, mFlipY );
    }

    public void flip( boolean x, boolean y )
    {
        mFlipX = x;
        mFlipY = y;
        super.flip( mFlipX, mFlipY );
    }
}

