package se.npi.trinity.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import se.npi.trinity.Camera;
import se.npi.trinity.model.WorldModel;
import se.npi.trinity.resources.Resources;

public class Text extends ARenderer {

    private String mText;
    private Matrix4 mNormalProjection;
    private BitmapFont mFont;

    public Text(String in, WorldModel model, Camera cam, SpriteBatch batch) {
        super(model, cam, batch);
        mNormalProjection = new Matrix4();
        mNormalProjection.setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        mText = in;
        mFont = Resources.INSTANCE.font;
        mFont.setColor(Color.RED);
    }

    @Override
    public void draw(float delta) {
        smSpriteBatch.begin();
        smSpriteBatch.setProjectionMatrix(mNormalProjection);
        mFont.draw(smSpriteBatch, Integer.toString(Gdx.graphics.getFramesPerSecond()), 100, 500);
        smSpriteBatch.end();
    }

    @Override
    public void dispose() {
        mFont.dispose();
    }
}
