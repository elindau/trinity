package se.npi.trinity.view;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import se.npi.trinity.Camera;
import se.npi.trinity.model.PhysicsWorld;
import se.npi.trinity.model.WorldModel;

public class Light extends ARenderer {

    private RayHandler mRayHandler;
    private PointLight mPlayerLight;

    private ConeLight mConeLight;
    private PointLight mPosLight;

    private Camera mCamera;

    public Light(WorldModel model, Camera cam, SpriteBatch batch) {
        super(model, cam, batch);

        mCamera = cam;

        RayHandler.setGammaCorrection(true);
        RayHandler.useDiffuseLight(true);
        mRayHandler = new RayHandler(PhysicsWorld.INSTANCE.world());
        mRayHandler.setAmbientLight(0.1f, 0.1f, 0.1f, 0.2f);
        mRayHandler.setCulling(true);

        mPlayerLight = new PointLight(mRayHandler, 99);
        mPlayerLight.attachToBody(model.getPlayer().getBody(), 0f, 0f);
        mPlayerLight.setColor(0f, 1f, 1f, 0.2f);
        mPlayerLight.setDistance(10f);

        // mConeLight = new ConeLight(mRayHandler, 99, Color.YELLOW, 10f, 10f, 10f, 90f, 90f);
    }

    @Override
    public void draw(float delta) {
        // Everything that's drawn before will be affected by the rayhandler
        mRayHandler.setCombinedMatrix(mCamera.combined);
        mRayHandler.updateAndRender();

        mPlayerLight.setDistance(MathUtils.random(9.5f, 10f));
    }

    @Override
    public void dispose() {
        mRayHandler.dispose();
    }

}
