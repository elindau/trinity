package se.npi.trinity.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import se.npi.trinity.Camera;
import se.npi.trinity.model.WorldModel;
import se.npi.trinity.resources.Resources;

public class WaterRenderer extends ARenderer {

    String vertexShader =
            "attribute vec4 a_position;    \n"
                    + "attribute vec2 a_texCoord0;\n"
                    + "uniform mat4 u_worldView;\n"
                    + "varying vec4 v_color;"
                    + "varying vec2 v_texCoords;"
                    + "void main()                  \n"
                    + "{                            \n"
                    + "   v_color = vec4(1, 1, 1, 1); \n"
                    + "   v_texCoords = a_texCoord0; \n"
                    + "   gl_Position =  u_worldView * a_position;  \n"
                    + "}                            \n";

    String fragmentShader = "#ifdef GL_ES\n"
            + "precision mediump float;\n"
            + "#endif\n"
            + "varying vec4 v_color;\n"
            + "varying vec2 v_texCoords;\n"
            + "uniform sampler2D u_texture;\n"
            + "uniform sampler2D u_texture2;\n"
            + "uniform float timedelta;\n"
            + "void main()                                  \n"
            + "{                                            \n"
            + "  vec2 displacement = texture2D(u_texture2, v_texCoords/6.0).xy;\n" //
            + "  float t=v_texCoords.y +displacement.y*0.1-0.15+  (sin(v_texCoords.x * 60.0+timedelta) * 0.005); \n" //
            + "  gl_FragColor = v_color * texture2D(u_texture, vec2(v_texCoords.x,t));\n"
            + "}";

    String fragmentShader2 = "#ifdef GL_ES\n"
            + "precision mediump float;\n"
            + "#endif\n"
            + "varying vec4 v_color;\n"
            + "varying vec2 v_texCoords;\n"
            + "uniform sampler2D u_texture;\n"
            + "void main()                                  \n"
            + "{                                            \n"
            + "  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);\n"
            + "}";

    private ShaderProgram mShader;
    private ShaderProgram mWaterShader;
    private Matrix4 mMatrix;
    private float mTime;
    private Mesh mWaterMesh;

    private Texture mWater = Resources.INSTANCE.water;
    private Texture mWaterDis = Resources.INSTANCE.waterDisplacement;

    public WaterRenderer(WorldModel model, Camera cam, SpriteBatch batch) {
        super(model, cam, batch);

        mWater.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        mWaterDis.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        mWaterDis.bind();

        mMatrix = new Matrix4();

        ShaderProgram.pedantic = false;

        mShader = new ShaderProgram(vertexShader, fragmentShader);
        mWaterShader = new ShaderProgram(vertexShader, fragmentShader2);

        int bottomY = 2;
        int bottomX = 28;
        int width = 11;
        int height = 7;
        mWaterMesh = createQuad(bottomX, bottomY, bottomX + width, bottomY, bottomX + width, bottomY + height, bottomX, bottomY + height);
        // mWaterMesh = createQuad(1, 1, 2, 2, 3, -0.3f, -1, -0.3f);

        mTime = 1f;
    }

    @Override
    public void draw(float delta) {
        mTime += delta;
        float angle = mTime * (2 * MathUtils.PI);
        if(angle > (2 * MathUtils.PI)) {
            angle -= (2 * MathUtils.PI);
        }

        // smSpriteBatch.setShader(mWaterShader);
        smSpriteBatch.begin();
        // mWaterShader.setUniformMatrix("u_worldView", smCamera.combined);
        smSpriteBatch.end();

        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        Gdx.gl20.glEnable(GL20.GL_BLEND);

        mWater.bind(1);
        mWaterDis.bind(2);

        mShader.begin();
        mShader.setUniformMatrix("u_worldView", smCamera.combined);
        mShader.setUniformi("u_texture", 1);
        mShader.setUniformi("u_texture2", 2);
        mShader.setUniformf("timedelta", -angle);
        mWaterMesh.render(mShader, GL20.GL_TRIANGLE_FAN);
        mShader.end();
    }

    public Mesh createQuad(float x1, float y1, float x2, float y2, float x3,
                           float y3, float x4, float y4) {
        float[] verts = new float[20];
        int i = 0;


        verts[i++] = x1; // x1
        verts[i++] = y1; // y1
        verts[i++] = 0;
        verts[i++] = 1f; // u1
        verts[i++] = 1f; // v1

        verts[i++] = x2; // x2
        verts[i++] = y2; // y2
        verts[i++] = 0;
        verts[i++] = 0f; // u2
        verts[i++] = 1f; // v2

        verts[i++] = x3; // x3
        verts[i++] = y3; // y2
        verts[i++] = 0;
        verts[i++] = 0f; // u3
        verts[i++] = 0f; // v3

        verts[i++] = x4; // x4
        verts[i++] = y4; // y4
        verts[i++] = 0;
        verts[i++] = 1f; // u4
        verts[i++] = 0f; // v4

        Mesh mesh = new Mesh(true, 4, 0, // static mesh with 4 vertices and no
                // indices
                new VertexAttribute(VertexAttributes.Usage.Position, 3,
                        ShaderProgram.POSITION_ATTRIBUTE), new VertexAttribute(
                VertexAttributes.Usage.TextureCoordinates, 2,
                ShaderProgram.TEXCOORD_ATTRIBUTE + "0"));

        mesh.setVertices(verts);
        return mesh;

    }

    @Override
    public void dispose() {

    }

}
