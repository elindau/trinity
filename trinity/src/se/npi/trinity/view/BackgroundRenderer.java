package se.npi.trinity.view;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import se.npi.trinity.Camera;
import se.npi.trinity.model.WorldModel;
import se.npi.trinity.model.entity.Player;
import se.npi.trinity.resources.Resources;

public class BackgroundRenderer extends ARenderer {

    private Sprite[] mParallaxBg;
    private Vector2 mOldPlayerPos, mOldCameraPos;
    private Player mPlayer;

    public BackgroundRenderer(WorldModel model, Camera cam, SpriteBatch batch) {
        super(model, cam, batch);

        mPlayer = model.getPlayer();
        mOldCameraPos = new Vector2();
        mOldPlayerPos = new Vector2();

        mParallaxBg = new Sprite[] {new Sprite(Resources.INSTANCE.background, 231, 63),
                new Sprite(Resources.INSTANCE.background, 231, 63),
                new Sprite(Resources.INSTANCE.background, 231, 63)};
        for (Sprite sprite : mParallaxBg) {
            sprite.setSize(Camera.CAMERA_WIDTH, Camera.CAMERA_HEIGHT);
        }
        mParallaxBg[0].setPosition(-Camera.CAMERA_WIDTH, 0);
        mParallaxBg[1].setPosition(0, 0);
        mParallaxBg[0].setPosition(Camera.CAMERA_WIDTH, 0);
    }

    @Override
    public void draw(float delta) {
        if(mParallaxBg[1].getX() > smCamera.position.x) {
            mParallaxBg[2].setPosition(mParallaxBg[0].getX() - Camera.CAMERA_WIDTH, mParallaxBg[2].getY());

            Sprite[] clone = mParallaxBg.clone();
            clone[0] = mParallaxBg[2];
            clone[1] = mParallaxBg[0];
            clone[2] = mParallaxBg[1];
            mParallaxBg = clone;
        }


        if(mParallaxBg[2].getX() + Camera.CAMERA_WIDTH < smCamera.position.x + Camera.CAMERA_WIDTH) {
            mParallaxBg[0].setPosition(mParallaxBg[2].getX() + Camera.CAMERA_WIDTH, mParallaxBg[0].getY());

            Sprite[] clone = mParallaxBg.clone();
            clone[0] = mParallaxBg[1];
            clone[1] = mParallaxBg[2];
            clone[2] = mParallaxBg[0];
            mParallaxBg = clone;
        }

        for(int i = 0; i < mParallaxBg.length; i++) {
            mParallaxBg[i].setPosition(mParallaxBg[i].getX(), smCamera.position.y - Camera.CAMERA_HEIGHT / 2);
            mParallaxBg[i].draw(smSpriteBatch);
        }

        mOldCameraPos = new Vector2(smCamera.position.cpy().x, smCamera.position.cpy().y);
        mOldPlayerPos = mPlayer.getPosition().cpy();
    }

    @Override
    public void dispose() {

    }
}
