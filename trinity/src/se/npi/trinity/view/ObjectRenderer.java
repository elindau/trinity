package se.npi.trinity.view;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import se.npi.trinity.Camera;
import se.npi.trinity.model.WorldModel;
import se.npi.trinity.model.entity.Tile;
import se.npi.trinity.resources.Resources;

import java.util.ArrayList;
import java.util.List;

public class ObjectRenderer extends ARenderer {

    private AnimatedSprite mCheckpoint;
    private Sprite mUnvisitedCheckpoint;
    private List<Tile> mObjects;

    public ObjectRenderer(WorldModel model, Camera cam, SpriteBatch batch) {
        super(model, cam, batch);

        mObjects = Resources.INSTANCE.mapHelper.getObjectTiles();
        Vector2 pos = new Vector2();
        for (Tile mObject : mObjects) {
            pos = mObject.readPosition;
        }
        mUnvisitedCheckpoint = Resources.INSTANCE.checkpointAtlas.createSprite("checkpoint", 2);
        mUnvisitedCheckpoint.setSize(1f, 1f);
        mUnvisitedCheckpoint.setPosition(pos.x, pos.y);

        mCheckpoint = new AnimatedSprite(Resources.INSTANCE.checkpointAtlas, "checkpoint", 2);
        mCheckpoint.setSize(1f, 1f);
        mCheckpoint.setPosition(pos.x, pos.y);
        mCheckpoint.stop();

        mCheckpoint.setAnimationRate(5);
        mCheckpoint.play();
        mCheckpoint.loop(true);
    }

    @Override
    public void draw(float delta) {
        boolean enabled = false;

        for (Tile mObject : mObjects) {
            if(mObject.enabled)
                enabled = true;
        }

        if(enabled) {
            mCheckpoint.update(delta);
            mCheckpoint.draw(smSpriteBatch);
        } else {
            mUnvisitedCheckpoint.draw(smSpriteBatch);
        }
    }

    @Override
    public void dispose() {

    }
}
