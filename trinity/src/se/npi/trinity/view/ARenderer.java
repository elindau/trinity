package se.npi.trinity.view;

import se.npi.trinity.Camera;
import se.npi.trinity.model.WorldModel;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class ARenderer {

	protected WorldModel smWorldModel;
	protected Camera smCamera;
	protected SpriteBatch smSpriteBatch;
	
	public ARenderer(WorldModel model, Camera cam, SpriteBatch batch) {
		smWorldModel = model;
		smCamera = cam;
		smSpriteBatch = batch;
	}

    public abstract void draw(float delta);
    public abstract void dispose();
}
