package se.npi.trinity;

import se.npi.trinity.model.TiledMapHelper;
import se.npi.trinity.model.entity.IEntity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

public class Camera extends OrthographicCamera {

	public static final float CAMERA_WIDTH = 40f;
	public static final float CAMERA_HEIGHT = 23f;
	
	private Vector2 mCameraLookAt;
	private IEntity mAttatching;
	private TiledMapHelper mMap;
	
	public Camera(TiledMapHelper map, float height, float width) {
		super(CAMERA_WIDTH, CAMERA_HEIGHT);
		
		mMap = map;
		mCameraLookAt = new Vector2(0f, 0f);
		mAttatching = null;
	}
	
	public boolean hasAttachedEntity() {
		return mAttatching == null ? false : true;
	}
	
	public void setLookAt(Vector2 val) {
		mCameraLookAt.x = val.x;
		mCameraLookAt.y = val.y;
	}
	
	public void setLookAt(float x, float y) {
		mCameraLookAt.x = x;
		mCameraLookAt.y = y;
	}
	
	public void attatchToEntity(IEntity ent) {
		mAttatching = ent;
	}
	
	public void detachEntity() {
		mAttatching = null;
	}
	
	public void update(float delta) {
		
		// Else manipulating lookatpos
		if(hasAttachedEntity()) {
            setLookAt(mAttatching.getPosition());
        }

		
		if(mCameraLookAt.x + CAMERA_WIDTH / 2f > mMap.getWidth()) {
			mCameraLookAt.x = mMap.getWidth() - CAMERA_WIDTH / 2f;
		} else if(mCameraLookAt.x - CAMERA_WIDTH / 2f < 0) {
			mCameraLookAt.x = CAMERA_WIDTH / 2f;
		}
		
		if(mCameraLookAt.y + CAMERA_HEIGHT / 2f > mMap.getHeight()) {
			mCameraLookAt.y = mMap.getHeight() - CAMERA_HEIGHT / 2f;
		} else if(mCameraLookAt.y - CAMERA_HEIGHT / 2f < 0) {
			mCameraLookAt.y = CAMERA_HEIGHT / 2f;
		}
		

		position.x = mCameraLookAt.x;
		position.y = mCameraLookAt.y;
		
		super.update();
	}
}
